import character.Char;
import weapons.Longbow;
import weapons.Staff;
import weapons.Weapon;

/**
 * Created by sjon on 8-5-17.
 */
public class ClashOfText {

    // The beginning of the application.
    // Here our application will start and create objects.
    public static void main(String[] args) {

        // Create a staff. Because Staff extends Weapon we can store it in a regular weapon
        Weapon staff = new Staff("Staff of glory");

        // Create a new character
        Char p1 = new Char("Bravos", 3, 30);
        p1.introduce();
        p1.equip(staff);


        // Create the enemy!!!
        Char p2 = new Char("Salazar", 2, 24);
        p2.introduce();
        // Note the difference with the other Character's equip method
        p2.equip(new Longbow("Longbow of Agamar"));

        // ATTACK!!!!
        p1.attack(p2);
        p2.attack(p1);
        p2.attack(p1);
        p2.attack(p1);
        p2.attack(p1);
        p2.attack(p1);

    }

}
