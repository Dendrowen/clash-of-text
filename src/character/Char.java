package character;

import weapons.Weapon;

/**
 * Created by sjon on 8-5-17.
 */
public class Char {

    // The private properties
    private int hitpointMax;
    private int level;
    private int hitpoints;
    private String name;
    private Weapon weapon;

    // Constructor
    public Char(String name, int level, int hitpoints){
        this.name = name;
        this.level = level;
        // At the beginning the HP en max HP are the same value. Therefor we don't need another argument hitpointMax.
        this.hitpointMax = hitpoints;
        this.hitpoints = hitpoints;
    }

    public void introduce(){
        System.out.println("My name is " + name + ". I am level " + level);
    }

    public void equip(Weapon weapon){
        System.out.println(name + " equiped " + weapon.getName());
        this.weapon = weapon;
    }

    public void attack(Char other){
        System.out.println("\n" + name + " attacks " + other.name);
        other.receiveDamage(weapon.getDamage());
    }

    // Handle the damage only if the character is alive.
    public void receiveDamage(int amount){
        if(isDead()){
            System.out.println(name + " already died. Don't be rude and leave the body at peace!");
        }
        else {

            hitpoints -= amount;
            System.out.println(name + " his hp is reduced by " + amount + " to " + hitpoints);

            if (isDead()) {
                System.out.println(name + " died.");
            }
        }
    }

    // Check if the character is dead
    public boolean isDead(){
        return hitpoints <= 0;
    }

    // Heal the character to full health
    public void heal(){
        hitpoints = hitpointMax;
        System.out.println(name + " his hitpoints are fully restored to " + hitpointMax);
    }

}
