package weapons;

/**
 * Created by sjon on 8-5-17.
 */
public class Axe extends Weapon{

    public Axe(String name){
        super(name, 6, 1);
    }

}
