package weapons;

/**
 * Created by sjon on 8-5-17.
 */
public class Dagger extends Weapon {

    public Dagger(String name){
        super(name, 3, 1);
    }

}
