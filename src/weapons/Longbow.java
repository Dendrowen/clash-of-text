package weapons;

/**
 * Created by sjon on 8-5-17.
 */
public class Longbow extends Weapon {

    public Longbow(String name){
        super(name, 6, 2);
    }

}
