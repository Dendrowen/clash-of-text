package weapons;

/**
 * Created by sjon on 8-5-17.
 */
public class Shortbow extends Weapon {

    public Shortbow(String name){
        super(name, 4, 2);
    }

}
