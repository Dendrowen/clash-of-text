package weapons;

/**
 * Created by sjon on 8-5-17.
 */
public class Shortsword extends Weapon{

    public Shortsword(String name){
        super(name, 4, 1);
    }

}
