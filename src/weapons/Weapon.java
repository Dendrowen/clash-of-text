package weapons;

import character.Char;

/**
 * Created by sjon on 8-5-17.
 */
public abstract class Weapon {

    // Properties
    private String name;
    private int damage;
    private int hands;
    private int range;

    public Weapon(String name, int damage, int hands){
        this.name = name;
        this.damage = damage;
        this.hands = hands;
    }

    // This is called a 'getter' and returns a certain value.
    // In OOP it is convention to make all the fields/properties private and make them accessible through these methods.
    public int getDamage(){
        return damage;
    }

    public String getName(){
        return name;
    }

}
